import {config, TemporaryCredentials} from 'aws-sdk';

// Need to update global variable before importing dynamo-orm
const prod = true;
config.update({
    region: "eu-west-1",
});
if (prod) {
    config.update({
        credentials: new TemporaryCredentials({
            RoleArn: "arn:aws:iam::953495156568:role/DevAccountRole",
            RoleSessionName: `purge-games-${new Date().getTime()}`
        })
    });
}

import {DeleteUtil} from "./delete-util";

//const len = prod ? "6dcf5468-0304-4e1f-af2e-962e81a30f72" : "665f7f7a-5f5d-4cdb-b927-35805c7f1074";
const skistar = prod ? "4bb191a0-6a53-4917-a1c4-33b3a88498bd": "\t7fc7d5a7-8fb0-4943-87a6-98596d81573e";
//const jonas = prod ? "a3771b2b-f73e-4942-ad12-6d7a677841d7" : "d3909b78-57ad-4cb8-bd2e-41a86d1e0b29";

//const untilDateEpochMicros = Date.parse("March 1, 2017") * 1000;
const untilDateEpochMicros = Date.parse("December 1, 2017") * 1000;
new DeleteUtil(skistar, untilDateEpochMicros).purgeGames(true);

