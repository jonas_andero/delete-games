import {DynamoDB} from 'aws-sdk';

import {Game} from './entities/game';

export class DeleteUtil {
    private static PAGE_SIZE: 50;

    constructor(private userId : string, private untilDateEpochMicros : number) {}

    private accumulateGames(exclusiveStartKey?: DynamoDB.DocumentClient.Key) : Promise < Game[] > {
        return Game
            .userIdStateIndex
            .query({hash: this.userId, limit: DeleteUtil.PAGE_SIZE, exclusiveStartKey})
            .then(result => {
                let games = result
                    .records
                    .filter(game => game.dateCreated <= this.untilDateEpochMicros
                        && ["created", "finished"].some(state => state == game.state));
                if (result.lastEvaluatedKey) {
                    return this.accumulateGames(result.lastEvaluatedKey)
                        .then(moreGames => {
                            let r = games.concat(moreGames);
                            return r;
                        });
                }
                return games;
            });
    }

    public async purgeGames(dryRun: boolean = false) {
        const games = await this.accumulateGames();
        if (dryRun) {
            console.log("gameId, state, title, date");
        }
        games
            .sort((g1, g2) => g1.dateCreated - g2.dateCreated)
            .forEach(game => {
            if (dryRun) {
                console.log(`${game.gameId}, ${game.state}, ${new Date(game.dateCreated / 1000).toISOString()}, ${game.title ? `"${game.title}"` : ""}`);
            }
        });
        if (!dryRun) {
            Game.writer.batchPut(games.map(game => {
                game.state = "purged";
                return game;
            }));
        }
    }
}
