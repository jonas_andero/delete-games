import { Table, Decorator, Query } from "dynamo-types";

@Decorator.Table({
    name: "spiideo-teams-games",
})
export class Game extends Table {
    @Decorator.Attribute()
    public gameId: string;

    @Decorator.Attribute()
    public userId: string;

    @Decorator.Attribute()
    public sceneId: string;

    @Decorator.Attribute()
    public dateCreated: number;

    @Decorator.Attribute()
    public state: string;

    @Decorator.Attribute()
    public title: string;

    @Decorator.HashPrimaryKey("gameId")
    public static readonly primaryKey: Query.HashPrimaryKey<Game, string>;

    @Decorator.HashGlobalSecondaryIndex('sceneId', { name: "sceneId-dateCreated-index" })
    static readonly sceneIdDateCreatedHash: Query.HashGlobalSecondaryIndex<Game, string>;

    @Decorator.FullGlobalSecondaryIndex('sceneId', 'dateCreated', { name: "sceneId-dateCreated-index" })
    static readonly sceneIdDateCreatedIndex: Query.FullGlobalSecondaryIndex<Game, string, number>;

    @Decorator.FullGlobalSecondaryIndex('userId', 'state', { name: "userId-state-index" })
    static readonly userIdStateIndex: Query.FullGlobalSecondaryIndex<Game, string, string>;

    @Decorator.Writer()
    static readonly writer: Query.Writer<Game>;
}
